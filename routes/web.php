<?php

use App\Models\Gif;
use Illuminate\Support\Facades\Http;
use Illuminate\Support\Facades\Route;
use function GuzzleHttp\json_decode;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $APIKEY = "2ujHJhK1z0BQNLL4RchgDnWhMatxUjAG";
    $url = 'https://api.giphy.com/v1/gifs/trending?api_key='.$APIKEY.'&limit=4';
    $client = Http::get($url);
    $result = json_decode($client->getBody(),true);
    $gif=  new Gif();
    $trending = $gif->getCustomData($result);
    return view('welcome')->with('trending',$trending);
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::get('search',[App\Http\Controllers\GifController::class,'search']);
Route::get('autoComplete',[App\Http\Controllers\GifController::class,'autoComplete']);
Route::get('gif/{id}',[App\Http\Controllers\GifController::class,'getItemByID']);
Route::get('trending',[App\Http\Controllers\GifController::class,'trending']);
