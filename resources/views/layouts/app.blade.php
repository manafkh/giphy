<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'GIPHY') }}</title>

    <!-- Scripts -->
{{-- --}}
    <!-- Font Icon -->
    <link rel="stylesheet" href="auth/fonts/material-icon/css/material-design-iconic-font.min.css">

    <!-- Main css -->
    <link rel="stylesheet" href="auth/css/style.css">
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js"></script>
    <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
    <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
{{--    <script src="{{asset('js/app.js')}}"></script>--}}
    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
{{--    <script src="auth/vendor/jquery/jquery.min.js"></script>--}}
{{--    <script src="auth/js/main.js"></script>--}}
</head>
<style>
    .autocomplete {
        /*the container must be positioned relative:*/
        position: relative;
        display: inline-block;
    }
    input {
        border: 1px solid transparent;
        background-color: #f1f1f1;
        padding: 10px;
        font-size: 16px;
    }
    input[type=text] {
        background-color: #f1f1f1;
        width: 100%;
    }
    input[type=submit] {
        background-color: DodgerBlue;
        color: #fff;
    }
    .autocomplete-items {
        position: absolute;
        border: 1px solid #d4d4d4;
        border-bottom: none;
        border-top: none;
        z-index: 99;
        /*position the autocomplete items to be the same width as the container:*/
        top: 100%;
        left: 0;
        right: 0;
    }
    .autocomplete-items div {
        padding: 10px;
        cursor: pointer;
        background-color: #fff;
        border-bottom: 1px solid #d4d4d4;
    }
    .autocomplete-items div:hover {
        /*when hovering an item:*/
        background-color: #e9e9e9;
    }
    .autocomplete-active {
        /*when navigating through the items using the arrow keys:*/
        background-color: DodgerBlue !important;
        color: #ffffff;
    }
    .loader-wrapper {
        display: none;
        width: 100%;
        height: 100%;
        position: absolute;
        top: 0;
        left: 0;
        background-color: #242f3f;

        justify-content: center;
        align-items: center;
    }
    .loader {
        display:inline-block ;
        width: 30px;
        height: 30px;
        position: relative;
        border: 4px solid #Fff;
        animation: loader 2s infinite ease;
    }
    .loader-inner {
        vertical-align: top;
        display: inline-block;
        width: 100%;
        background-color: #fff;
        animation: loader-inner 2s infinite ease-in;
    }
    @keyframes loader {
        0% { transform: rotate(0deg);}
        25% { transform: rotate(180deg);}
        50% { transform: rotate(180deg);}
        75% { transform: rotate(360deg);}
        100% { transform: rotate(360deg);}
    }
    @keyframes loader-inner {
        0% { height: 0%;}
        25% { height: 0%;}
        50% { height: 100%;}
        75% { height: 100%;}
        100% { height: 0%;}
    }

</style>
<body class="">
    <div id="app">
        <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
            <div class="container-fluid">
                <a class="navbar-brand" href="{{ url('/') }}">{{ config('app.name', 'Giphy') }}</a>
                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                        <li class="nav-item">
                            <a class="nav-link active" aria-current="page" href="#">Home</a>
                        </li>
                        @guest
                            @if (Route::has('login'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                                </li>
                            @endif

                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }}
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                            <div class="form-inline">
                                <select class="form-control" name="limit" id="limit">
                                    <option value="10">10</option>
                                    <option value="20">20</option>
                                    <option value="40">40</option>
                                    <option value="50">50</option>
                                </select>
                                <div class="autocomplete" style="width:300px;">
                                    <input id="search" name="search" type="text" class="form-control me-2" placeholder="Search" />
                                </div>
                                <button id="btn_search" class="btn btn-outline-success me-2" type="submit">Search</button>
                            </div>
                        @endguest

                    </ul>
                </div>
            </div>
        </nav>

        <main>
            @yield('content')
        </main>
    </div>
    <script type="text/javascript">
        var CSRF_TOKEN = $('meta[name="csrf-token"]').attr('content');
        let current_offset = 1;
        let retrieved = false;
        $(document).ready(function() {
            $("#search").autocomplete({

                source: function (request, response) {
                    $.ajax({
                        url: "{{url('autoComplete')}}",
                        data: {
                            term: request.term
                        },
                        dataType: "json",
                        success: function (data) {
                            var resp = $.map(data, function (obj) {
                                return obj.name;
                            });

                            response(resp);
                        }
                    });
                },
                minLength: 2
            });


            function getResults(offset){
                $value = $('#search').val();
                $limit = $('#limit').find(":selected").val();
                $offset = offset;
                $.ajax({
                    type: 'get',
                    url: '{{url('search')}}',
                    data: {
                        'search': $value,
                        'limit': $limit,
                        'offset': $offset
                    },
                    dataType: 'JSON',
                    success: function (data) {
                        retrieved = true;
                        let len = data.data.length;
                        for (var i = 0; i < len; i++) {
                            let htmlShowImage = '<div class="col-md-3 line-content" style="width: 200px;height: 200px"> <div class="thumbnail"> <a href="#" target="_blank"> <img src="%src%" alt="Lights" class="img-thumbnail" > <div class="caption"> <p>%title%</p> </div> </a> </div> </div>';

                            let newHtml = htmlShowImage.replace('%src%', data.data[i].url);
                            newHtml = newHtml.replace('%title%', data.data[i].title);
                            document.querySelector('.show_giphy').insertAdjacentHTML('beforeend', newHtml);
                        }
                    },
                    showPage:function (){

                    }
                });
            }

                $('#btn_search').on('click', function () {
                   getResults(current_offset);
                });

            $(window).scroll(function() {
                if(Math.ceil($(window).scrollTop() + $(window).height()) === $(document).height()) {
                    retrieved = false;
                    $(".loader-wrapper").css('display','flex');
                    $(".loader-wrapper").show()
                    setTimeout(function (){

                        if (!retrieved){
                            $(".loader-wrapper").hide()
                            getResults(++current_offset);
                        }

                    },6000)

                    console.log(current_offset);
                }
            });
        });

    </script>
</body>
</html>
