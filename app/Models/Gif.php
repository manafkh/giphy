<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use phpDocumentor\Reflection\DocBlock\Tags\Var_;

class Gif extends Model
{
    use HasFactory;



    public function getCustomData($response): array
    {
        $data = collect();
        $item[] = [];
        foreach ($response['data'] as $value){
            $item =[
                'id'=> $value['id'] ,
                'title'=>$value['title'],
                'url'=>$value['images']['downsized']['url'],
                'source'=>$value['source']
                ];
            $data->push($item);
        }
        $customResponse = [
            'data'=> $data,
            'pagination'=>[
                'total_count'=> $response['pagination']['total_count'],
                'count'=>$response['pagination']['count'],
                'offset'=>$response['pagination']['offset']
            ],
        ];


        return $customResponse;
    }
    public function getAutoCompleteWord($response): \Illuminate\Support\Collection
    {
        $customResponse = collect();
        $item[] = [];
        foreach ($response['data'] as $value){
            $item =[
                'name'=>$value['name']
            ];
            $customResponse->push($item);
        }



        return $customResponse;
    }
}
