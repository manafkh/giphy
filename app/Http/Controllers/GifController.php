<?php

namespace App\Http\Controllers;

use App\Http\Resources\GifResource;
use App\Models\Gif;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Http;
use function GuzzleHttp\json_decode;

class GifController extends Controller
{
 public function __Constructor(){
     $this->middleware('auth');

    }
    public function search(Request $request): \Illuminate\Http\JsonResponse
    {
         $APIKEY = env('APP_KEY_GIPHY');
         $url = 'https://api.giphy.com/v1/gifs/search?api_key='.$APIKEY.'&limit='.$request->limit.'&q='.$request->search.'&offset='.$request->offset;
         $client = Http::get($url);
         $result = json_decode($client->getBody(),true);
         $gif=  new Gif();
          return  response()->json( $gif->getCustomData($result));

    }

    public function autoComplete(Request $request): \Illuminate\Support\Collection
    {
        $APIKEY = env('APP_KEY_GIPHY');
        $url = 'https://api.giphy.com/v1/gifs/search/tags?api_key='.$APIKEY.'&limit=5&q='.$request->get('term');
        $client = Http::get($url);
        $result = json_decode($client->getBody(),true);
        $autoComplete=  new Gif();
        return $autoComplete->getAutoCompleteWord($result);
    }

    public function getItemByID($id){
        $APIKEY = env('APP_KEY_GIPHY');
        $url = 'https://api.giphy.com/v1/gifs?api_key='.$APIKEY.'&ids='.$id;
        $client = Http::get($url);
        $result = json_decode($client->getBody(),true);
        return $result;
    }

}
